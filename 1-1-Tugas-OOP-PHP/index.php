<?php

trait Hewan{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi(){
        echo "{$this->nama} sedang {$this->keahlian}";
    }
}

abstract class Fight{
    use Hewan;
    public $attackPower;
    public $defencePower;
    
    public function serang($hewan){
        echo "{$this->nama} sedang menyerang {$hewan->nama}";

        $hewan->diserang($this);
    }

    public function diserang($hewan){
        echo "<br>{$this->nama} sedang diserang {$hewan->nama}";

        $this->darah = $this->darah - ( $hewan->attackPower / $this->defencePower ); 
    }

    protected function getInfo(){
        echo "Nama : {$this->nama}";
        echo "<br>";
        echo "darah : {$this->darah}";
        echo "<br>";
        echo "jumlahKaki : {$this->jumlahKaki}";
        echo "<br>";
        echo "keahlian : {$this->keahlian}";
        echo "<br>";
        echo "attackPower : {$this->attackPower}";
        echo "<br>";
        echo "defencePower : {$this->defencePower}";
        echo "<br>";
        $this->atraksi();
    }
    abstract public function getInfoHewan();
}

class Elang extends Fight{
    public function __construct()
    {
        $this->nama = "Elang";
        $this->jumlahKaki = 2;
        $this->keahlian = "Terbang Tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }

    public function getInfoHewan(){
        $this->getInfo();
    }
}

class Harimau extends Fight{
    public function __construct()
    {
        $this->nama = "Harimau";
        $this->jumlahKaki = 4;
        $this->keahlian = "Lari Cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }
    
    public function getInfoHewan(){
        $this->getInfo();
    }
}

class Line{
    public static function baris(){
        echo "<br>";
        echo "======================";
        echo "<br>";
    }
}

$elang = new Elang();
$elang->getInfoHewan();

Line::baris();

$harimau = new Harimau();
$harimau->getInfoHewan();

Line::baris();

$harimau->serang($elang);

$elang->getInfoHewan();

Line::baris();

$harimau = new Harimau();
$harimau->getInfoHewan();

Line::baris();