// soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

daftarHewan.sort();

for (let i = 0; i < daftarHewan.length; i++) {
  console.log(daftarHewan[i]);
}

// soal 2
function introduce(bio) {
  return "Nama saya " + bio["name"] + ", umur saya " + bio["age"] + " tahun, alamat saya di " + bio["address"] + ", dan saya punya hobby yaitu " + bio["hobby"] + "!";
}

var data = { name: "John", age: 30, address: "Jalan Pelesiran", hobby: "Gaming" };

var perkenalan = introduce(data);
console.log(perkenalan);

// soal 3
function hitung_huruf_vokal(str) {
  const count = str.match(/[aeiou]/gi).length;

  return count;
}

var hitung_1 = hitung_huruf_vokal("Muhammad");

var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1, hitung_2);

// soal 4
function hitung(int) {
  return int * 2 - 2;
}

console.log(hitung(0)); // -2
console.log(hitung(1)); // 0
console.log(hitung(2)); // 2
console.log(hitung(3)); // 4
console.log(hitung(5)); // 8
