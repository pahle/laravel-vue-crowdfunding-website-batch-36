
# Tugas Akhir Laravel Vue Sanbercode Batch 36

Membuat Aplikasi Crowdfunding menggunakan Laravel dan Vue JS


## Demo
https://youtu.be/eCmxuhpqqGI


## Screenshots
##### Home
![Home](demo/home.jpg)



##### All Campaigns
![All Campaigns](demo/allcampaign.jpg)



##### Detail Campaign
![Detail Campaign](demo/detailcampaign.jpg)



##### Search
![Search](demo/search.jpg)



##### Login
![Login](demo/login.jpg)



##### User Sidebar
![Login](demo/sidebaruser.jpg)



##### Guest Sidebar
![Login](demo/sidebarguest.jpg)



