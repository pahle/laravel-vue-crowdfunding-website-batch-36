// soal 1
const luasPersegiPanjang = (p, l) => {
  return p * l;
};

console.log(luasPersegiPanjang(5, 4));

const kelilingPersegiPanjang = (p, l) => {
  return 2 * (p + l);
};

console.log(kelilingPersegiPanjang(5, 4));

// soal 2
// const newFunction = function literal(firstName, lastName) {
//   return {
//     firstName: firstName,
//     lastName: lastName,
//     fullName: function () {
//       console.log(firstName + " " + lastName);
//     },
//   };
// };

// //Driver Code
// newFunction("William", "Imoh").fullName();

const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => {
      console.log(`${firstName} ${lastName}`);
    },
  };
};

newFunction("William", "Imoh").fullName();

// soal 3
var newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};

const { firstName, lastName, address, hobby } = newObject;

console.log(firstName, lastName, address, hobby);

// soal 4
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];
//Driver Code
console.log(combined);

// soal 5
const planet = "earth";
const view = "glass";
var after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;

console.log(after);
