<!doctype html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  </head>
  <body style="font-family: sans-serif;">
    <div style="display: block; margin: auto; max-width: 600px;" class="main">
      <h1 style="font-size: 18px; font-weight: bold; margin-top: 20px">Selamat {{$name}} code OTP berhasil di generate ulang!</h1>
      <p>Berikut adalah OTP code yang baru.</p>
      <div style="background-color: orange">
          <h3 style="color: white; text-align:center;">{{$otp}}</h3>
      </div>
      <p>OTP Code akan kadaluarsa dalam 5 menit, Gunakan Segera</p>
    </div>
    <!-- Example of invalid for email html/css, will be detected by Mailtrap: -->
    <style>
      .main { background-color: white; }
      a:hover { border-left-width: 1em; min-height: 2em; }
    </style>
  </body>
</html>