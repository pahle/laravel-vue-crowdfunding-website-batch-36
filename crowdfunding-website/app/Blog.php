<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UseUuid;

class Blog extends Model
{
    use UseUuid;

    protected $guarded = [];
}
