<?php

namespace App\Http\Middleware;

use Closure;

class RoleVerifiedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();

        if($user->role_id == 'bc1006ac-de27-460b-9b1d-d35a3c7828e4'){
            return $next($request);
        }
        
        return response()->json([
            'message' => 'Role anda tidak memiliki izin mengakses halaman'
        ]);
    }
}