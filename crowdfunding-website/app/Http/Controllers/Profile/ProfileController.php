<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;

class ProfileController extends Controller
{
    public function show(Request $request)
    {
        return response()->json([
            'response_code' => '00',
            'response_message' => 'Profile berhasil ditampilkan',
            'data' => auth()->user(),
        ]);
    }

    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'photo_profile' => 'image|mimes:jpeg,png,jpg',
        ]);

        $user = User::where('id', auth()->user()->id)->first();

        if(!$user){
            return response()->json([
                'response_code' => '01',
                'response_message' => 'User tidak ditemukan',
            ]);
        }

        $path = '/images/users/photo-profile/';
        $imageName = Str::slug($user->name, '-') . '-' . time() . '.' . $request->photo_profile->extension();
        $request->photo_profile->move(public_path($path), $imageName);
        $imgAddress = $path . $imageName;

        $user->update([
            'name' => $request->name,
            'photo_profile' => $imgAddress,
        ]);

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Profile berhasil di update',
            'data' => $user,
        ]);
    }
}