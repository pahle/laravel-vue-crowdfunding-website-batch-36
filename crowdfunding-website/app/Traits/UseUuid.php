<?php

namespace App\Traits;

use illuminate\Support\Str;

trait UseUuid
{
    public function getIncrementing(){
        return false;
    }

    public function getKeyType(){
        return 'string';
    }

    public static function bootUseUuid()
    {
        static::creating(function($model){
            if(empty($model->{$model->getKeyName})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }
}

?>