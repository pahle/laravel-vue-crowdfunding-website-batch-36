<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UseUuid;

class Campaign extends Model
{
    use UseUuid;

    protected $guarded = [];
}
